#!/bin/sh

args_without_cat_files_and_final=""

dockerfile="Dockerfile"
while test $# -gt 1; do
    case "$1" in
        -f|--file)
            shift
            dockerfile=$1
            shift
            ;;
        -f=*)
            dockerfile=${1#"-f="}
            shift
            ;;
        --file=*)
            dockerfile=${1#"--file="}
            shift
            ;;
        *)
	    args_without_cat_files_and_final="$args_without_cat_files_and_final $1 "
	    shift
            ;;
    esac
done
lockfile="$dockerfile-lock"

echo "Build from $lockfile"
docker build $args_without_cat_files_and_final --file $lockfile "$@"
